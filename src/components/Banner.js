// base imports
import React from 'react';

// bootstrap components
// import Jumbotron from 'react-bootstrap/Jumbotron';
// import Button from 'react-bootstrap/Button';

// bs grid system
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

// shortcut
import {Jumbotron, Button, Row, Col} from 'react-bootstrap';

// export default allows the function to be used in other files
export default function Banner(){
	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>Jumbotron Title</h1>
					<p> Banner description </p>
					<Button variant="primary">Enroll Now</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}