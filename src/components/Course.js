// base imports
import React, {useState, useEffect} from 'react';

import PropTypes from 'prop-types';

// bootstrap
import {Card, Button} from 'react-bootstrap';

export default function Course({course}){
	// console.log(course)
	// const {name, description, price} = course;

	// use state hook for this component to be able to store its state
	const[count, setCount] = useState(0);
	// use state hook for getting and setting the seats for this course
	const[seats, setSeats] = useState(10);
	// state hook that indicates availability of course for enrollment
	const[isOpen, setIsOpen] = useState(true);

	// check the availability for enrollment of a course
	useEffect(() =>{
		if(seats === 0){
			setIsOpen(false);
		}
	}, [seats]); //is OPTIONAL parameter
	// react will re-run this effect ONLY if any of the values contained in this array has changed from the last render/update

	function enroll(){
		setCount(count + 1);
		setSeats(seats - 1);
	}

	if(course){
		return(
			<Card>
				<Card.Body>
					<Card.Title> {course.name} </Card.Title>
					<Card.Text>
						<h5>Description</h5>
						{course.description}
						<h5>Price</h5>
						{course.price}
						<h5>Enrolee: </h5>
						{count}
						<h5>Seats: </h5>
						{seats}
					</Card.Text>
					{
						isOpen ? <Button className="bg-primary" onClick = {enroll}>Enroll!</Button> 
						:
						<Button className="bg-secondary" disabled>Enroll!</Button>
					}
					
				</Card.Body>
			</Card>
		)
	} else {
		return "";
	}
}

Course.propTypes = {
	//shape() is used to check that the prop object conforms to a specific "shape"
	course: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}