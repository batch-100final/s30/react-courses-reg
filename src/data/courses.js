export default [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "First Description",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Second Description",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Third Description",
		price: 60000,
		onOffer: true
	}
]