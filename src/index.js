// base imports
import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// bootstrap components
import Container from 'react-bootstrap/Container';

// app components
import AppNavbar from './components/Navbar';

// page components
import Home from './pages/Home';
import Course from './pages/Course';
import Register from './pages/Register';

// reactDom render the component on the root div
ReactDOM.render(
	<Fragment>
	    <AppNavbar />
	    <Home />
	   	<Container>
		    <Register />
	    </Container>
    </Fragment>,
  document.getElementById('root')
  );

