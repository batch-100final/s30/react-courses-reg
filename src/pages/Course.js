// base import
import React from 'react';

// app components
import Course from '../components/Course';

// app data
import coursesData from '../data/courses';

// export default allows the function to be used in other files
export default function Courses(){
	const courses = coursesData.map(course => {
		return(
			<Course key = {course.id} course = {course} />
		)
	})
	return(
		<React.Fragment>
			{courses}
		</React.Fragment>
	)
}


