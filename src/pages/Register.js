// base imports
import React from 'react';

// bootstrap
import {Form, Button} from 'react-bootstrap';

// export default allows the function to be used in other files
export default function Register(){
	return(
		<Form>
			<Form.Group controlId = "userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "xxx@xxx.com"
					required
				/>
				<Form.Text className = "text-muted">
					We will never share your email address with anyone.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId = "Password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Password"
					required
				/>
			</Form.Group>

			<Form.Group controlId = "Password2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type = "Password"
					placeholder = "Confirm Password"
					required
				/>
			</Form.Group>
			<Button className="bg-primary" type="submit" id="submitBtn">Submit</Button>
		</Form>
	)
}