//Course pages

// base imports
import React, {Fragment} from 'react';
import Proptypes from 'prop-types';

// app components
import Course from '../components/Course';

// bootstrap components
import Container from 'react-bootstrap/Container';

// 
import courses from '../data/courses';

export default function Courses(){
	// create multiple course components to the content of coursesData
	const CourseCards = courses.map((course) => {
		return(
			<Course course={course} />
		);
	})
	return(
		<Fragment>
		<Container>
			{CourseCards}
		</Container>
		</Fragment>
	)
}

// Course components

// base imports
import React from 'react';

import {Card, Button} from 'react-bootstrap';

export default function Course({ course }){
	console.log(course);

	if(course){
		return(
			<Card>
				<Card.Body>
					<Card.Title> {course.name} </Card.Title>
					<Card.Text>
						<span className="subtitle">Description: </span>
						 <br />
						 {course.description}
						 <br />
						<span className="subtitle">Price: </span>
						PhP {course.price}
						 <br />
					</Card.Text>
					<Button variant="primary">Enroll!</Button>
				</Card.Body>
			</Card>
		)
	} else {
		return "";
	}
}